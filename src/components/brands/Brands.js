import React from "react";
import car from "../../assets/images/ffff.png";
import monitor from "../../assets/images/mac.png";
import gplay from "../../assets/images/play.png";

const Brands = () => {
  return (
    <div>
      <div className="container ">
        <div className="row d-flex align-items-center bg-padding">
          <div className="col-md-6 pdt-30 brands">
            <h1>
              BRAND <span>ADVERTISEMET</span>
            </h1>
            <p>
              {" "}
              <small>
                The meteoric rise of Uber,Pathao, and Obhai others has created a
                new class of vehicles that garner high exposure in front of your
                target audience. Creative, memorable, and technologically
                advanced, CAR ADS is redefining out-of-home.
              </small>
            </p>
            <button type="button" className="btn driver">
              Brands Login
            </button>
            <button type="button" className="btn driver">
              Brands Registration
            </button>
          </div>
          <div className="text-right col-md-6 car-head pdt-60">
            <img src={car} alt="" />
          </div>
        </div>

        <div className="row mt-80">
          <div className="col-md-7">
            <img className="img-fluid" src={monitor} alt="" />
          </div>
          <div className="col-md-5 carVar">
            <h2>
              CARDADS DRIVER AS <br /> <span>ADVERTISING PLATFORM</span>
            </h2>
            <p>
              We don’t just wrap vehicles. Our Mobile App Based Ad Tech Platform
              will make you the controller of your campaign.
            </p>
            <p className="mt-3">
              With our user friendly Mobile App and Web App Dashboard you can
              easily track your wrapped vehicle’s real time route,distance,
              analytics with our revolutionary Impression Count Algorithm.
            </p>
            <p className="mt-3">
              We take the responsibility on behalf of Brands to advertise their
              products or services more effectively and efficiently than any
              other marketing platforms in Bangladesh by using tech.
            </p>
            <img src={gplay} alt="" />{" "}
            <span className="ml-12 downLoad_ins">DOWNLOAD THE APP NOW</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Brands;
