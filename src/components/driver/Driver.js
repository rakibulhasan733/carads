import React from "react";
import car2 from "../../assets/images/car2.png";

const Driver = () => {
  return (
    <div className="container">
      <div className="row d-flex align-items-center bg-padding">
        <div className="col-md-6 pdt-30 brands">
          <h1>
            Car Ads for Driver <br /> <span>Drive & Earn</span>
          </h1>
          <p>
            {" "}
            <small>
              Join the thousands of drivers who are earning monthly income with
              us by placing an advertisement on your car.
            </small>
          </p>
          <button type="button" className="btn driver">
            Driver Login
          </button>
          <button type="button" className="btn driver">
            Driver Registration
          </button>
        </div>
        <div className="text-right col-md-6 car-head pdt-60">
          <img src={car2} alt="" />
        </div>
      </div>
    </div>
  );
};

export default Driver;
