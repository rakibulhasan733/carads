import React from "react";
import "./Footer.css";
import phone from "../../assets/images/footer/phone-call.png";
import envelap from "../../assets/images/footer/envelope.png";
import home from "../../assets/images/footer/home.png";
import logo from "../../assets/images/logo.png";

const Footer = () => {
    return (
        <footer className="footer-bg">
            <div className="container">
                <div className="row">
                    <div className="col-md-5 intro">
                        <img src={logo} alt="" id="int" />
                        <p>
                            <strong> CAR ADS</strong> is the international{" "}
                            <strong> OOH DOOH</strong> in bangladesh to create effective brand
                            advertising on moving vehicles
                        </p>
                        {/* <button type="button" className="btn footer_driver">
              Talk to the Team
            </button>
            <button type="button" className="btn footer_adviser">
              Become a Driver
            </button> */}
                    </div>

                    <div className="col-md-2 links">
                        <h4>SERVICES</h4>
                        <p>
                            <a href="false">Drive with us</a>
                        </p>
                        <p>
                            <a href="false">advertise with us</a>
                        </p>
                        <p>
                            <a href="false">Wrap for us</a>
                        </p>
                        <p>
                            <a href="false">Franchise</a>
                        </p>
                    </div>

                    <div className="col-md-2 links">
                        <h4>USEFUL LINKS</h4>

                        <p>
                            <a href="false">About Us</a>
                        </p>
                        <p>
                            <a href="false">Career</a>
                        </p>
                        <p>
                            <a href="false">Terms & Condtions</a>
                        </p>
                        <p>
                            <a href="false">Privacy Policy</a>
                        </p>
                    </div>
                    <div className="col-md-3 links">
                        <h4>CONTACT</h4>
                        <p>
                            <img src={envelap} alt="" />
                            &nbsp;&nbsp; info@carads.com.bd
                        </p>
                        <div className="mt-3 d-flex">
                            <p>  <img src={phone} alt="" /></p>
                            <p className="company_location">
                                +88 02 4108 0999; <br />
                                +88 096 7808 0999;<br />
                                +88 019 9999 1022
                            </p>

                        </div>
                        <div>
                            <div className="mt-3 d-flex">
                                <p> <img src={home} alt="" /></p>
                                <div className="company_location">
                                   <p> <span> Corporate Head Office:</span> <br />
                                    House-82/1, Road-02, Block-A, Niketon, Gulshan-01, Dhaka-1212.{" "}</p>
                                   

                                    <div className="mt-2"><p>Corporate Drivers Hub: <br />
                                    H#14,R#14, Block-D, Mirpur-06, Dhaka-1216.</p></div>

                                


                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                </div>
            </div>
        </footer>
    );
};

export default Footer;
