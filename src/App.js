import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Navbar from "./components/navbar/Navbar";

import "../src/assets/css/Style.css";

import "bootstrap/dist/css/bootstrap.min.css";
import Footer from "./components/footer/Footer";
import HomePage from "./components/hompage/HomePage";
import Brands from "./components/brands/Brands";
import Driver from "./components/driver/Driver";

function App() {
  return (
    <div className="App">
        <Navbar />
      <Router>
      
        
          <Switch>
            <Route exact path="/">
              <HomePage />
            </Route>
            <Route path="/home">
              <HomePage />
            </Route>
            <Route path="/brands">
              <Brands />
            </Route>
            <Route path="/driver">
              <Driver />
            </Route>
          </Switch>
         
   
      </Router>
      <Footer />
    </div>
  );
}

export default App;
